#include <chrono>
#include <iostream>
#include <list>
#include <ranges>
#include <thread>

#include <arpa/inet.h>
#include <execinfo.h>

#include <common/executor.hpp>
#include <common/game.hpp>
#include <common/log.hpp>
#include <common/messages.hpp>
#include <common/net_client.hpp>
#include <common/network_executor.hpp>
#include <common/task.hpp>

class NetServer {
 public:
  NetServer(NetworkExecutor &exec) : exec{exec} {}

  ~NetServer() {
    posix_unwrap(close(socket), "close");
  }

  void listen() {
    socket = posix_unwrap(::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0),
                          "socket");

    in_addr ip_addr;
    int res = inet_pton(AF_INET, "127.0.0.1", &ip_addr);
    if (res == 0) {
      throw std::runtime_error{"Failed to parse address"};
    }
    sockaddr_in addr{
        .sin_family = AF_INET,
        .sin_port = 1337,
        .sin_addr = ip_addr,
        .sin_zero = {},
    };
    exec.listen(socket, addr, 4);
  }

  Task<std::pair<std::error_code, int>> accept() {
    co_return co_await exec.async_accept(socket);
  }

 private:
  int socket = -1;
  NetworkExecutor &exec;
};

class GameServer {
 public:
  Task<void> client_routine(Executor &exec, int socket) {
    NetClient client{exec.network(), socket};

    auto [err, msg_bytes] = co_await client.receive_msg();
    if (err == std::errc::connection_reset) {
      co_return;
    }
    LOG(Server, INFO, "Received greeting: {}", msg_bytes);
    auto msg = deserialize<GreetingMessage>(msg_bytes);
    LOG(Server, INFO, "Greeting: player id {}", msg.player.id);

    state.players[msg.player.id] = msg.player;

    auto update_send_loop = [&]() -> Task<void> {
      while (true) {
        GameUpdateMessage msg;
        msg.players.resize(state.players.size());
        std::ranges::copy(std::views::values(state.players), msg.players.begin());
        auto msg_bytes = serialize(msg);
        auto err = co_await client.send_msg(msg_bytes);
        if (err == std::errc::connection_reset
            || err == std::errc::broken_pipe) {
          co_return;
        }

        co_await sleep(exec.timer(), std::chrono::seconds(1));
      }
    };
    exec.dispatch(update_send_loop());

    auto update_receive_loop = [&]() -> Task<void> {
      while (true) {
        auto [err, msg_bytes] = co_await client.receive_msg();
        if (err == std::errc::connection_reset) {
          co_return;
        }
        auto msg = deserialize<PlayerInputMessage>(msg_bytes);
        switch(msg.action.index()) {
          case 0: {
            auto& move = std::get<0>(msg.action);
            auto& player = state.players[msg.id];
            auto offset = move.direction * FIXED_DELTA_TIME * player.speed; 
            state.players[msg.id].pos += offset;
            break;
          }
          case 1:
            throw std::runtime_error{"Shoot action is not implemented"};
        }
      }
    };
    exec.dispatch(update_receive_loop());
  }

 private:
  void apply_input(const PlayerInputMessage &msg) {
    switch (msg.action.index()) {
      case 0: {
        auto &move = std::get<0>(msg.action);
        auto &player = state.players[msg.id];
        player.pos.x += move.direction.x * FIXED_DELTA_TIME * player.speed;
        break;
      }
      case 1:
        break;
    }
  }

  static constexpr float FIXED_DELTA_TIME = 1.0 / 60.0;
  GameState state;
};

#ifdef TRACE_ALLOCATIONS

void *operator new(std::size_t n) {
  auto *p = std::malloc(n);
  std::cout << "\n\nallocate " << n << " -> " << p << "\n";
  void *buffer[100];
  int num = backtrace(buffer, 100);
  backtrace_symbols_fd(buffer, num, 0);
  return p;
}

void operator delete(void *p) {
  std::cout << "deallocate " << p << "\n";
  return std::free(p);
}

void operator delete(void *p, std::size_t) {
  std::cout << "deallocate " << p << "\n";
  return std::free(p);
}

#endif

int main() {
  std::cout << std::format("{}", std::vector<uint8_t>{0, 1, 2, 3, 15}) << "\n"; 
  assert(std::format("{}", std::vector<uint8_t>{0, 1, 2, 3, 15}) == "000102030f");

  Executor executor;

  NetServer listener{executor.network()};
  listener.listen();

  GameServer server;

  auto loop = [&]() -> Task<void> {
    while (true) {
      auto [err, client_socket] = co_await listener.accept();
      if (err) {
        std::cerr << "Failed to accept client: " << err.message() << "\n";
        continue;
      }
      std::cout << "Accepted a client\n";
      executor.dispatch(server.client_routine(executor, client_socket));
    }
  };

  executor.dispatch(loop());

  while (true) {
    executor.update();
    std::this_thread::yield();
  }
}
