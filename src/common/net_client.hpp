#pragma once

#include <common/network_executor.hpp>
#include <common/task.hpp>

class NetClient {
 public:
  explicit NetClient(NetworkExecutor &exec) : exec{exec} {}
  explicit NetClient(NetworkExecutor &exec, int socket)
      : socket{socket}, exec{exec} {
    assert(socket > 0);
  }

  ~NetClient() {
    posix_unwrap(close(socket), "close");
  }

  Task<std::error_code> connect() {
    socket = ::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    in_addr ip;
    posix_unwrap(inet_pton(AF_INET, "127.0.0.1", &ip));
    sockaddr_in addr{
        .sin_family = AF_INET,
        .sin_port = 1337,
        .sin_addr = ip,
        .sin_zero = {},
    };
    co_return co_await exec.async_connect(socket, addr);
  }

  Task<std::pair<std::error_code, std::vector<uint8_t>>> receive_msg() {
    auto [err, size_bytes] = co_await exec.async_receive(socket, 4);
    if (err) {
      co_return {err, {}};
    }

    uint32_t size = static_cast<uint32_t>(size_bytes[0])
                  | (static_cast<uint32_t>(size_bytes[1]) << 8)
                  | (static_cast<uint32_t>(size_bytes[2]) << 16)
                  | (static_cast<uint32_t>(size_bytes[3]) << 24);

    std::vector<uint8_t> msg;
    msg.reserve(size);
    while (msg.size() != size) {
      auto [err, msg_part] = co_await exec.async_receive(socket, size);
      if (err) {
        co_return {err, {}};
      }
      msg.insert(msg.end(), msg_part.begin(), msg_part.end());
    }
    co_return {std::error_code{}, std::move(msg)};
  }

  Task<std::error_code> send_msg(std::vector<uint8_t> data) {
    uint8_t size_bytes[4]{
        static_cast<uint8_t>(data.size()),
        static_cast<uint8_t>(data.size() >> 8),
        static_cast<uint8_t>(data.size() >> 16),
        static_cast<uint8_t>(data.size() >> 24),
    };
    auto [err, sent_bytes] = co_await exec.async_send(
        socket, std::vector<uint8_t>(size_bytes, size_bytes + 4));
    if (err) {
      co_return err;
    }
    assert(sent_bytes == 4);

    sent_bytes = 0;
    auto data_size = data.size();
    while (sent_bytes != data_size) {
      auto [err, more_sent_bytes] = co_await exec.async_send(socket, data);
      if (err) {
        co_return err;
      }
      sent_bytes += more_sent_bytes;
      data.erase(data.begin(), data.begin() + sent_bytes);
    }
    co_return std::error_code{};
  }

 private:
  int socket = -1;
  NetworkExecutor &exec;
};
