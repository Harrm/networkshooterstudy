#pragma once

#include <format>
#include <limits>
#include <span>
#include <vector>
#include <cstring>

#include <common/game.hpp>
#include <common/log.hpp>
#include <common/reflection.hpp>

template <>
struct std::formatter<std::vector<uint8_t>> {
  using ParseContext = std::format_parse_context;
  using FormatContext = std::format_context;

  constexpr auto parse(const ParseContext &ctx) {
    return ctx.end() - 1;
  }

  template <typename FormatContext>
  auto format(const std::vector<uint8_t> &arg, FormatContext &ctx) const {
    for (uint8_t n : arg) {
      std::format_to(ctx.out(), "{:02x}", n);
    }
    return ctx.out();
  }
};

template <typename T>
struct std::formatter<std::vector<T>> {
  using ParseContext = std::format_parse_context;
  using FormatContext = std::format_context;

  constexpr auto parse(const ParseContext &ctx) {
    return ctx.end() - 1;
  }

  template <typename FormatContext>
  auto format(const std::vector<T> &arg, FormatContext &ctx) const {
    for (const T& n : arg) {
      std::format_to(ctx.out(), "{}, ", n);
    }
    return ctx.out();
  }
};

template <typename... Options>
struct std::formatter<std::variant<Options...>> {
  using ParseContext = std::format_parse_context;
  using FormatContext = std::format_context;

  constexpr auto parse(const ParseContext &ctx) {
    return ctx.end() - 1;
  }

  template <typename FormatContext>
  auto format(const std::variant<Options...> &arg, FormatContext &ctx) const {
    std::visit([&](const auto& option) {
      std::format_to(ctx.out(), "{}", option);
    }, arg);
    return ctx.out();
  }
};

template <Reflected T>
struct std::formatter<T> {
  using ParseContext = std::format_parse_context;
  using FormatContext = std::format_context;

  constexpr auto parse(const ParseContext &ctx) {
    return ctx.end() - 1;
  }

  template <typename FormatContext>
  auto format(const T &arg, FormatContext &ctx) const {
    std::format_to(ctx.out(), "{} {{", T::TYPENAME);
    visit_named_fields(arg, [&](std::string_view field_name, const auto &field) {
      std::format_to(ctx.out(), "{}: {}, ", field_name, field);
    });
    std::format_to(ctx.out(), "}}");
    return ctx.out();
  }
};

struct WriteStream {
  void write(uint8_t byte) {
    data.push_back(byte);
  }

  std::vector<uint8_t> data;
};

struct ReadStream {
  uint8_t read() {
    assert(!unread.empty());
    auto v = unread[0];
    unread = unread.subspan(1);
    return v;
  }

  std::span<const uint8_t> unread;
};

void serialize_to(uint64_t n, WriteStream &out) {
  for (int i = 0; i < 8; i++) {
    out.write(n & 0xFF);
    n >>= 8;
  }
}

void serialize_to(uint32_t n, WriteStream &out) {
  for (int i = 0; i < 4; i++) {
    out.write(n & 0xFF);
    n >>= 8;
  }
}

void serialize_to(float n, WriteStream &out) {
  static_assert(std::numeric_limits<float>::is_iec559);
  static_assert(sizeof(float) == 4);
  uint8_t *n_bytes = reinterpret_cast<uint8_t *>(&n);
  for (size_t i = 0; i < sizeof(float); i++) {
    out.write(n_bytes[i]);
  }
}

void serialize_to(std::string_view s, WriteStream &out) {
  serialize_to(s.size(), out);
  for (auto c : s) {
    out.write(c);
  }
}

template <typename T>
void serialize_to(const std::vector<T> &v, WriteStream &out) {
  serialize_to(std::span(v), out);
}

template <typename T>
void serialize_to(std::span<const T> v, WriteStream &out) {
  serialize_to(v.size(), out);
  for (auto &i : v) {
    serialize_to(i, out);
  }
}

template <Reflected T>
void serialize_to(const T &v, WriteStream &out) {
  visit_named_fields(v, [&out](std::string_view field_name, auto &field) {
    LOG(Serialization, TRACE, "Serialize field {} = {}", field_name, field);
    serialize_to(field, out);
  });
}

template <typename... Options>
void serialize_to(const std::variant<Options...> &v, WriteStream &out) {
  static_assert(sizeof...(Options) <= 255);
  out.write(v.index());
  std::visit([&out](const auto &option) { serialize_to(option, out); }, v);
}

void deserialize_from(uint64_t &n, ReadStream &in) {
  n = 0;
  for (int i = 0; i < 8; i++) {
    n = n | (in.read() << (i*8));
  }
}

void deserialize_from(float &n, ReadStream &in) {
  static_assert(std::numeric_limits<float>::is_iec559);
  static_assert(sizeof(float) == 4);
  uint8_t n_bytes[sizeof(float)];
  for (size_t i = 0; i < sizeof(float); i++) {
    n_bytes[i] = in.read();
  }
  std::memcpy(&n, n_bytes, sizeof(float));
}

void deserialize_from(std::string &s, ReadStream &in) {
  size_t size;
  deserialize_from(size, in);
  s.resize(size);
  for (auto &c : s) {
    c = in.read();
  }
}

template <typename T>
void deserialize_from(std::vector<T> &v, ReadStream &in) {
  size_t size;
  deserialize_from(size, in);
  v.resize(size);
  for (size_t i = 0; i < size; i++) {
    deserialize_from(v[i], in);
  }
}

template <typename... Options>
using Deserializer = void (*)(std::variant<Options...> &, ReadStream &);

template <size_t I = 0, size_t N, typename... Options>
consteval void generate_deserializer(
    std::array<Deserializer<Options...>, N> &array) {
  array[I] = [](std::variant<Options...> &v, ReadStream &in) {
    v = std::variant_alternative_t<I, std::variant<Options...>>{};
    deserialize_from(std::get<I>(v), in);
  };
  if constexpr (I < N - 1) {
    generate_deserializer<I + 1, N, Options...>(array);
  }
}

template <typename... Options, size_t N = sizeof...(Options)>
consteval std::array<Deserializer<Options...>, N> generate_deserializers() {
  std::array<Deserializer<Options...>, N> arr;
  generate_deserializer(arr);
  return arr;
}

template <typename... Options>
void deserialize_from(std::variant<Options...> &v, ReadStream &in) {
  static_assert(sizeof...(Options) <= 255);
  uint8_t idx = in.read();
  constexpr std::array<Deserializer<Options...>, sizeof...(Options)>
      deserializers = generate_deserializers<Options...>();
  deserializers[idx](v, in);
}

template <Reflected T>
void deserialize_from(T &v, ReadStream &in) {
  visit_named_fields(v, [&in](std::string_view field_name, auto &field) {
    deserialize_from(field, in);
    LOG(Deserialization, TRACE, "Deserialized field {} = {}", field_name, field);
  });
}

template <typename T>
concept Serializable = requires(T t, WriteStream out) {
  { serialize_to(t, out) };
};

template <typename T>
concept Deserializable =
    std::is_default_constructible_v<T> && requires(T &t, ReadStream in) {
      { deserialize_from(t, in) };
    };

template <Serializable T>
std::vector<uint8_t> serialize(const T &msg) {
  WriteStream out;

  serialize_to(msg, out);

  return out.data;
}

template <Deserializable T>
T deserialize(std::span<const uint8_t> data) {
  ReadStream in{data};

  T t;
  deserialize_from(t, in);
  return t;
}

// client to server
struct GreetingMessage {
  REFLECT(GreetingMessage, Player, player);
};

// server to client
struct GameUpdateMessage {
  REFLECT(GameUpdateMessage, std::vector<Player>, players);
};

// client to server
struct PlayerInputMessage {
  struct Move {
    REFLECT(Move, Vector, direction);
  };

  struct Shoot {
    REFLECT(Shoot, Vector, mouse_pos);
  };
  using Action = std::variant<Move, Shoot>;
  REFLECT(PlayerInputMessage, uint64_t, id, Action, action);
};
