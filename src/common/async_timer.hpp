#pragma once

#include <chrono>
#include <coroutine>
#include <list>

struct [[nodiscard]] TimerAwaitible {
  using Clock = std::chrono::steady_clock;

  bool await_ready() const {
    return duration == Clock::duration::zero();
  }

  void await_suspend(std::coroutine_handle<> handle);
  void await_resume() {}

  class TimerExecutor &exec;
  Clock::time_point start;
  Clock::duration duration;
  std::coroutine_handle<> handle{};
};

class TimerExecutor {
 public:
  using Clock = std::chrono::steady_clock;

  void add_timer(TimerAwaitible timer) {
    pending_timers.emplace_back(std::move(timer));
  }

  void update() {
    auto now = Clock::now();
    for (auto timer_it = pending_timers.begin();
         timer_it != pending_timers.end();
         timer_it++) {
      auto &timer = *timer_it;
      if (now - timer.start > timer.duration) {
        timer.handle.resume();
        timer_it = pending_timers.erase(timer_it);
      }
    }
  }

 private:
  std::list<TimerAwaitible> pending_timers;
};

void TimerAwaitible::await_suspend(std::coroutine_handle<> handle) {
  this->handle = handle;
  exec.add_timer(*this);
}

template <typename Rep, typename Period>
TimerAwaitible sleep(TimerExecutor &exec,
                     std::chrono::duration<Rep, Period> duration) {
  return TimerAwaitible{exec, TimerAwaitible::Clock::now(), duration};
}
