

template <typename T>
class Generator {
 public:
  Generator(const Generator&) = delete;
  Generator& operator=(const Generator&) = delete;

  Generator(Generator&&) = delete;
  Generator& operator=(Generator&&) = delete;

  ~Generator() {
    handle.destroy();
  }

  struct promise_type {
    std::suspend_never initial_suspend() {
      return {};
    }

    std::suspend_always final_suspend() noexcept {
      is_over = true;
      return {};
    }

    void unhandled_exception() {
      std::terminate();
    }

    std::suspend_always yield_value(T &&v) {
      value = std::move(v);
      return {};
    }

    Generator get_return_object() {
      return Generator{
          std::coroutine_handle<promise_type>::from_promise(*this)};
    }

    T value;
    bool is_over = false;
  };

  explicit Generator(std::coroutine_handle<promise_type> handle)
      : handle{handle} {}

  struct Iterator {
    Iterator(bool is_end, Generator &generator)
        : is_end{is_end}, generator{generator} {}

    Iterator(Generator &generator)
        : is_end{generator.handle.promise().is_over}, generator{generator} {}

    Iterator &operator++() {
      generator.handle.resume();
      is_end = generator.handle.promise().is_over;
      return *this;
    }

    T &operator*() {
      return generator.handle.promise().value;
    }

    bool operator==(const Iterator &other) const {
      return is_end && other.is_end;
    }

    bool is_end{};
    Generator &generator;
  };

  Iterator begin() {
    return Iterator{*this};
  }

  Iterator end() {
    return Iterator{true, *this};
  }

 private:
  std::coroutine_handle<promise_type> handle;
};