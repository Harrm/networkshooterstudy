#pragma once

#include <cassert>
#include <coroutine>
#include <exception>

struct PromiseBase {
  std::suspend_always initial_suspend() {
    return {};
  }

  auto final_suspend() noexcept {
    struct FinalAwaiter {
      bool await_ready() const noexcept {
        return !suspended_handle;
      }

      std::coroutine_handle<> await_suspend(std::coroutine_handle<>) noexcept {
        if (suspended_handle) {
          assert(!suspended_handle.done());
          return suspended_handle;
        } else {
          return std::noop_coroutine();
        }
      }

      void await_resume() noexcept {}

      std::coroutine_handle<> suspended_handle;
    };

    return FinalAwaiter{suspended_handle};
  }

  void unhandled_exception() {
    try {
      std::rethrow_exception(std::current_exception());
    } catch (std::exception &e) {
      std::cerr << e.what() << "\n";
      std::abort();
    }
  }

  std::coroutine_handle<> suspended_handle = nullptr;
};

class [[nodiscard]] TaskHandle {
 public:
  virtual ~TaskHandle() = default;

  virtual void resume() = 0;
  virtual bool is_done() const = 0;
};

template <typename Promise>
class TaskBase : public TaskHandle {
 public:
  TaskBase(std::coroutine_handle<Promise> handle) : handle{handle} {}

  TaskBase(const TaskBase &) = delete;
  TaskBase &operator=(const TaskBase &) = delete;

  TaskBase(TaskBase &&task) {
    handle = task.handle;
    task.handle = nullptr;
  }

  TaskBase &operator=(TaskBase &&task) {
    destroy();
    handle = task.handle;
    task.handle = nullptr;
  }

  virtual ~TaskBase() {
    std::move(*this).destroy();
  }

  void destroy() && {
    if (handle != nullptr) {
      if (!handle.done()) {
        throw std::runtime_error{"Destroying a coroutine that is not done"};
      }
      handle.destroy();
    }
  }

  void resume() override {
    assert(handle && !handle.done());
    handle.resume();
  }

  bool is_done() const override {
    assert(handle);
    return handle.done();
  }

  bool await_ready() const {
    return false;
  }

  auto await_suspend(std::coroutine_handle<> suspended_handle) {
    assert(this->handle);
    handle.promise().suspended_handle = suspended_handle;
    return handle;
  }

 protected:
  std::coroutine_handle<Promise> handle;
};

template <typename T>
struct Promise;

template <typename T = void>
class Task : public TaskBase<Promise<T>> {
 public:
  using return_type = T;

  using promise_type = Promise<T>;

  Task(std::coroutine_handle<promise_type> handle) : TaskBase<Promise<T>> {
    handle
  }
  {}

  T await_resume() {
    assert(this->handle);
    return this->handle.promise().value.value();
  }
};

template <>
class Task<void> : public TaskBase<Promise<void>> {
 public:
  using return_type = void;
  using promise_type = Promise<void>;

  Task(std::coroutine_handle<promise_type> handle) : TaskBase{handle} {}

  void await_resume() {}
};

template <typename T>
struct Promise : public PromiseBase {
  void return_value(T value) {
    this->value = std::move(value);
  }

  Task<T> get_return_object() {
    return Task<T>{std::coroutine_handle<Promise<T>>::from_promise(*this)};
  }

  std::optional<T> value;
};

template <>
struct Promise<void> : public PromiseBase {
  void return_void() {}

  Task<void> get_return_object() {
    return Task<void>{
        std::coroutine_handle<Promise<void>>::from_promise(*this)};
  }
};
