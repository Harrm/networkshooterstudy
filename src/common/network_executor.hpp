#pragma once

#include <coroutine>
#include <functional>
#include <system_error>

#include <netinet/in.h>
#include <sys/socket.h>

#include "epoll_service.hpp"

class NetworkExecutor {
 public:  
  using ReceiveResult = std::pair<std::error_code, std::vector<uint8_t>>;
  using ReceiveCallback = std::function<void(ReceiveResult)>;
  void async_receive(int socket, size_t size, ReceiveCallback cb) {
    waiting_tasks.emplace(
        SocketEvent{socket, Event::READ}, [size, socket, cb = std::move(cb)]() {
          std::vector<uint8_t> data(size);
          int res = recv(socket, data.data(), data.size(), 0);
          LOG(Network, TRACE, "recv on socket {} -> {} (errno {})", socket, res, errno);
          if (res != -1) {
            data.resize(res);
            cb(std::make_pair(std::error_code{}, std::move(data)));
          } else {
            cb(std::make_pair(
                std::make_error_code(static_cast<std::errc>(errno)),
                std::vector<uint8_t>{}));
          }
        });
  }

  auto async_receive(int socket, size_t size) {
    struct ReceiveAwaitible {
      bool await_ready() const {
        return false;
      }

      void await_suspend(std::coroutine_handle<> handle) {
        exec.async_receive(socket, size, [this, handle](ReceiveResult res) {
          result = std::move(res);
          handle.resume();
        });
      }

      std::pair<std::error_code, std::vector<uint8_t>> await_resume() {
        return std::move(result);
      }

      int socket;
      size_t size;
      NetworkExecutor &exec;
      std::pair<std::error_code, std::vector<uint8_t>> result;
    };
    return ReceiveAwaitible{socket, size, *this, {}};
  }

  using SendResult = std::pair<std::error_code, uint32_t>;
  using SendCallback = std::function<void(SendResult)>;
  void async_send(int socket, std::vector<uint8_t> data, SendCallback cb) {
    waiting_tasks.emplace(
        SocketEvent{socket, Event::WRITE},
        [data = std::move(data), socket, cb = std::move(cb)]() {
          int res = send(socket, data.data(), data.size(), MSG_NOSIGNAL);
          LOG(Network, TRACE, "send on socket {} -> {} (errno {})", socket, res, errno);
          if (res != -1) {
            cb(std::make_pair(std::error_code{}, res));
          } else {
            cb(std::make_pair(
                std::make_error_code(static_cast<std::errc>(errno)), 0));
          }
        });
  }

  auto async_send(int socket, std::vector<uint8_t> data) {
    struct SendAwaitible {
      bool await_ready() const {
        return false;
      }

      void await_suspend(std::coroutine_handle<> handle) {
        exec.async_send(
            socket, std::move(data), [this, handle](SendResult res) {
              result = std::move(res);
              handle.resume();
            });
      }

      SendResult await_resume() {
        return std::move(result);
      }

      int socket;
      std::vector<uint8_t> data;
      NetworkExecutor &exec;
      SendResult result;
    };
    return SendAwaitible{socket, std::move(data), *this, {}};
  }

  using ConnectCallback = std::function<void(std::error_code)>;
  void async_connect(int socket, sockaddr_in addr, ConnectCallback cb) {
    int res =
        ::connect(socket, reinterpret_cast<sockaddr *>(&addr), sizeof(addr));
    if (res == 0) {
      cb(std::make_error_code(std::errc{}));
      return;
    }
    if (res == -1 && errno == EINPROGRESS) {
      service.add_socket(socket);
      waiting_tasks.emplace(
          SocketEvent{socket, Event::WRITE}, [socket, cb = std::move(cb)]() {
            int error = 0;
            socklen_t error_size = sizeof(error);
            posix_unwrap(
                getsockopt(socket, SOL_SOCKET, SO_ERROR, &error, &error_size));
            cb(std::make_error_code(static_cast<std::errc>(error)));
          });
    } else {
      cb(std::make_error_code(static_cast<std::errc>(errno)));
    }
  }

  auto async_connect(int socket, sockaddr_in addr) {
    struct ConnectAwaitible {
      bool await_ready() const {
        return false;
      }

      void await_suspend(std::coroutine_handle<> handle) {
        exec.async_connect(socket, addr, [this, handle](std::error_code err) {
          res = err;
          handle.resume();
        });
      }

      std::error_code await_resume() {
        return res;
      }

      int socket;
      sockaddr_in addr;
      NetworkExecutor &exec;
      std::error_code res{};
    };
    return ConnectAwaitible{socket, addr, *this};
  }

  void listen(int socket, sockaddr_in addr, int backlog) {
    posix_unwrap(
        ::bind(socket, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)),
        "bind");

    posix_unwrap(::listen(socket, backlog), "listen");
    service.add_socket(socket);
  }

  using AcceptResult = std::pair<std::error_code, int>;
  using AcceptCallback = std::function<void(std::error_code, int)>;
  void async_accept(int socket, AcceptCallback cb) {
    waiting_tasks.emplace(
        SocketEvent{socket, Event::READ}, [socket, this, cb = std::move(cb)]() {
          int res = accept4(socket, nullptr, nullptr, SOCK_NONBLOCK);
          if (res != -1) {
            service.add_socket(res);
            cb(std::error_code{}, res);
          } else {
            cb(std::make_error_code(static_cast<std::errc>(errno)), -1);
          }
        });
  }

  auto async_accept(int socket) {
    struct AcceptAwaitible {
      bool await_ready() const {
        return false;
      }

      void await_suspend(std::coroutine_handle<> handle) {
        exec.async_accept(socket, [this, handle](std::error_code err, int client_socket) {
          result = std::make_pair(err, client_socket);
          handle.resume();
        });
      }

      AcceptResult await_resume() {
        return std::move(result);
      }

      int socket;
      NetworkExecutor &exec;
      AcceptResult result;
    };
    return AcceptAwaitible{socket, *this, {}};
  }

  void update() {
    for (auto ready_socket : service.get_ready_sockets()) {
      auto [start, end] = waiting_tasks.equal_range(ready_socket);
      for (auto it = start; it != end; it++) {
        it->second();
      }
      waiting_tasks.erase(start, end);
    }
  }

 private:
  EpollService service;
  std::unordered_multimap<SocketEvent, std::function<void()>> waiting_tasks;
};
