#pragma once

#include <cstddef>
#include <type_traits>

template <size_t N_>
struct Tag {
  static constexpr size_t N = N_;
};

template <size_t Idx = 0, typename Visitor, typename Arg, typename... Args>
void apply_to_all(Visitor &&v, Arg &arg, Args &...args) {
  v(Tag<Idx>{}, arg);
  apply_to_all<Idx + 1>(std::forward<Visitor>(v), args...);
}

template <size_t Idx = 0, typename Visitor, typename Arg, typename... Args>
void apply_to_all(Visitor &&v, const Arg &arg, const Args &...args) {
  v(Tag<Idx>{}, arg);
  apply_to_all<Idx + 1>(std::forward<Visitor>(v), args...);
}

template <size_t Idx, typename Visitor>
void apply_to_all(Visitor &&) {}

#define DEFINE_VISIT_FIELDS(n, ...)                                \
  template <typename T, typename Visitor>                          \
  constexpr void visit_fields_n(Tag<n>, T &t, Visitor &&v) {       \
    auto &[__VA_ARGS__] = t;                                       \
    apply_to_all(std::forward<Visitor>(v), __VA_ARGS__);           \
  }                                                                \
  template <typename T, typename Visitor>                          \
  constexpr void visit_fields_n(Tag<n>, const T &t, Visitor &&v) { \
    auto &[__VA_ARGS__] = t;                                       \
    apply_to_all(std::forward<Visitor>(v), __VA_ARGS__);           \
  }

DEFINE_VISIT_FIELDS(1, a);
DEFINE_VISIT_FIELDS(2, a, b);
DEFINE_VISIT_FIELDS(3, a, b, c);
DEFINE_VISIT_FIELDS(4, a, b, c, d);
DEFINE_VISIT_FIELDS(5, a, b, c, d, e);
DEFINE_VISIT_FIELDS(6, a, b, c, d, e, f);
DEFINE_VISIT_FIELDS(7, a, b, c, d, e, f, g);

template <typename T>
concept Reflected = std::is_aggregate_v<T>
                 && std::same_as<decltype(T::FIELD_NUM), const size_t>;

template <Reflected T, typename Visitor>
constexpr void visit_fields(T &t, Visitor &&v) {
  visit_fields_n(Tag<T::FIELD_NUM>{}, t, [&](auto tag, auto &field_value) {
    std::forward<Visitor>(v)(field_value);
  });
}
template <Reflected T, typename Visitor>
constexpr void visit_fields(const T &t, Visitor &&v) {
  visit_fields_n(Tag<T::FIELD_NUM>{}, t, [&](auto tag, auto &field_value) {
    std::forward<Visitor>(v)(field_value);
  });
}

template <Reflected T, typename Visitor>
constexpr void visit_named_fields(T &t, Visitor &&v) {
  visit_fields_n(Tag<T::FIELD_NUM>{}, t, [&](auto tag, auto &field_value) {
    constexpr size_t Idx = decltype(tag)::N;
    constexpr std::string_view FieldName = T::FIELD_NAMES[Idx];
    std::forward<Visitor>(v)(FieldName, field_value);
  });
}
template <Reflected T, typename Visitor>
constexpr void visit_named_fields(const T &t, Visitor &&v) {
  visit_fields_n(Tag<T::FIELD_NUM>{}, t, [&](auto tag, auto &field_value) {
    constexpr size_t Idx = decltype(tag)::N;
    constexpr std::string_view FieldName = T::FIELD_NAMES[Idx];
    std::forward<Visitor>(v)(FieldName, field_value);
  });
}

template <typename T>
struct ReflectionData;

struct ReflectionField {
  std::string_view name;
};

#define DECLARE_FIELD(type, name) type name;

#define COUNT(_1,     \
              _2,     \
              _3,     \
              _4,     \
              _5,     \
              _6,     \
              _7,     \
              _8,     \
              _9,     \
              _10,    \
              _11,    \
              _12,    \
              _13,    \
              _14,    \
              _15,    \
              _16,    \
              _17,    \
              _18,    \
              _19,    \
              TARGET, \
              ...)    \
  TARGET
#define COUNT_ARGS(...) \
  COUNT(__VA_ARGS__,    \
        19,             \
        18,             \
        17,             \
        16,             \
        15,             \
        14,             \
        13,             \
        12,             \
        11,             \
        10,             \
        9,              \
        8,              \
        7,              \
        6,              \
        5,              \
        4,              \
        3,              \
        2,              \
        1)
static_assert(COUNT_ARGS(a, b, c) == 3);

#define IDENT(...) __VA_ARGS__

#define COMMA ,

#define CONCAT__(a, b) a##b
#define CONCAT_(a, b) CONCAT__(a, b)
#define CONCAT(a, b) CONCAT_(IDENT(a), IDENT(b))

#define TAKE_ODD_0()
#define TAKE_ODD_1(a) a
#define TAKE_ODD_2(a, b) a
#define TAKE_ODD_3(a, b, ...) a, TAKE_ODD_1(__VA_ARGS__)
#define TAKE_ODD_4(a, b, ...) a, TAKE_ODD_2(__VA_ARGS__)
#define TAKE_ODD_5(a, b, ...) a, TAKE_ODD_3(__VA_ARGS__)
#define TAKE_ODD_6(a, b, ...) a, TAKE_ODD_4(__VA_ARGS__)
#define TAKE_ODD_7(a, b, ...) a, TAKE_ODD_5(__VA_ARGS__)
#define TAKE_ODD_8(a, b, ...) a, TAKE_ODD_6(__VA_ARGS__)
#define TAKE_ODD_9(a, b, ...) a, TAKE_ODD_7(__VA_ARGS__)
#define TAKE_ODD_10(a, b, ...) a, TAKE_ODD_8(__VA_ARGS__)
#define TAKE_ODD(...) CONCAT(TAKE_ODD_, COUNT_ARGS(__VA_ARGS__))(__VA_ARGS__)

#define TAKE_EVEN_0()
#define TAKE_EVEN_1(a)
#define TAKE_EVEN_2(a, b) b
#define TAKE_EVEN_3(a, b, ...) b, TAKE_EVEN_1(__VA_ARGS__)
#define TAKE_EVEN_4(a, b, ...) b, TAKE_EVEN_2(__VA_ARGS__)
#define TAKE_EVEN_5(a, b, ...) b, TAKE_EVEN_3(__VA_ARGS__)
#define TAKE_EVEN_6(a, b, ...) b, TAKE_EVEN_4(__VA_ARGS__)
#define TAKE_EVEN_7(a, b, ...) b, TAKE_EVEN_5(__VA_ARGS__)
#define TAKE_EVEN_8(a, b, ...) b, TAKE_EVEN_6(__VA_ARGS__)
#define TAKE_EVEN_9(a, b, ...) b, TAKE_EVEN_7(__VA_ARGS__)
#define TAKE_EVEN_10(a, b, ...) b, TAKE_EVEN_8(__VA_ARGS__)
#define TAKE_EVEN(...) CONCAT(TAKE_EVEN_, COUNT_ARGS(__VA_ARGS__))(__VA_ARGS__)

static_assert(std::string_view{TAKE_EVEN("a", "b", "c")} == "b");

#define STRINGIFY_0()
#define STRINGIFY_1(a) #a
#define STRINGIFY_2(a, ...) #a, STRINGIFY_1(__VA_ARGS__)
#define STRINGIFY_3(a, ...) #a, STRINGIFY_2(__VA_ARGS__)
#define STRINGIFY_4(a, ...) #a, STRINGIFY_3(__VA_ARGS__)
#define STRINGIFY_5(a, ...) #a, STRINGIFY_4(__VA_ARGS__)
#define STRINGIFY_6(a, ...) #a, STRINGIFY_5(__VA_ARGS__)
#define STRINGIFY_7(a, ...) #a, STRINGIFY_6(__VA_ARGS__)
#define STRINGIFY_8(a, ...) #a, STRINGIFY_7(__VA_ARGS__)
#define STRINGIFY_9(a, ...) #a, STRINGIFY_8(__VA_ARGS__)
#define STRINGIFY_10(a, ...) #a, STRINGIFY_9(__VA_ARGS__)
#define STRINGIFY_N(...) \
  CONCAT(STRINGIFY_, COUNT_ARGS(__VA_ARGS__))(__VA_ARGS__)

#define DECLARE_FIELDS_0()
#define DECLARE_FIELDS_2(type, name, ...) type name;
#define DECLARE_FIELDS_4(type, name, ...) \
  type name;                              \
  DECLARE_FIELDS_2(__VA_ARGS__)
#define DECLARE_FIELDS_6(type, name, ...) \
  type name;                              \
  DECLARE_FIELDS_4(__VA_ARGS__)
#define DECLARE_FIELDS_8(type, name, ...) \
  type name;                              \
  DECLARE_FIELDS_6(__VA_ARGS__)
#define DECLARE_FIELDS_10(type, name, ...) \
  type name;                               \
  DECLARE_FIELDS_8(__VA_ARGS__)
#define DECLARE_FIELDS_12(type, name, ...) \
  type name;                               \
  DECLARE_FIELDS_10(__VA_ARGS__)
#define DECLARE_FIELDS_14(type, name, ...) \
  type name;                               \
  DECLARE_FIELDS_12(__VA_ARGS__)
#define DECLARE_FIELDS(...) \
  CONCAT(DECLARE_FIELDS_, COUNT_ARGS(__VA_ARGS__))(__VA_ARGS__)

#define REFLECT(type, ...)                                               \
 public:                                                                 \
  static constexpr std::string_view TYPENAME = #type;                    \
  static constexpr size_t FIELD_NUM = COUNT_ARGS(TAKE_ODD(__VA_ARGS__)); \
  static constexpr std::array<std::string_view, FIELD_NUM> FIELD_NAMES{  \
      STRINGIFY_N(TAKE_EVEN(__VA_ARGS__))};                              \
  DECLARE_FIELDS(__VA_ARGS__)
