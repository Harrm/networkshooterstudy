#pragma once

#include <common/async_timer.hpp>
#include <common/network_executor.hpp>
#include <common/task.hpp>

class Executor {
 public:
  NetworkExecutor &network() {
    return net_exec;
  }

  TimerExecutor &timer() {
    return timer_exec;
  }

  template<typename T> requires std::derived_from<T, TaskHandle>
  void dispatch(T&& task) {
    auto ptr = std::make_unique<T>(std::move(task));
    scheduled_coroutines.emplace_back(std::move(ptr));
  }

  void update() {
    for (auto &coro : scheduled_coroutines) {
      coro->resume();
      if (!coro->is_done()) {
        running_coroutines.emplace_back(std::move(coro));
      }
    }
    scheduled_coroutines.clear();

    timer_exec.update();
    net_exec.update();
  }

 private:
  std::vector<std::unique_ptr<TaskHandle>> scheduled_coroutines;
  std::vector<std::unique_ptr<TaskHandle>> running_coroutines;
  NetworkExecutor net_exec;
  TimerExecutor timer_exec;
};

class SurrenderAwaitible {
 public:
  bool await_ready() {
    return false;
  }

  template<typename T>
  void await_suspend(std::coroutine_handle<Promise<T>> coro) {
    exec.dispatch(Task<T>(coro));
  }

  void await_resume() {}

  Executor &exec;
};

// yields the coroutine to give other coroutines the chance to run
SurrenderAwaitible surrender(Executor &exec) {
  return SurrenderAwaitible{exec};
}
