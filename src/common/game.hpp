#pragma once

#include <cassert>
#include <chrono>
#include <cmath>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <common/reflection.hpp>

struct Color {
  REFLECT(Color, uint64_t, r, uint64_t, g, uint64_t, b);
};

struct Vector {
  REFLECT(Vector, float, x, float, y);

  Vector &operator+=(Vector rhs) {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }

  Vector operator*(Vector rhs) const {
    return Vector{x * rhs.x, y * rhs.y};
  }

  Vector operator*(float rhs) const {
    return Vector{x * rhs, y * rhs};
  }
};

struct Player {
  REFLECT(Player,
          uint64_t,
          id,
          std::string,
          name,
          Color,
          color,
          Vector,
          pos,
          float,
          speed);
};

struct GameState {
  std::unordered_map<uint64_t, Player> players;
};
