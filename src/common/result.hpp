#pragma once

#include <variant>

template <typename V, typename Err>
class Result {
 public:
  Result(V &&value) : res{value} {}
  Result(Err &&error) : res{error} {}

  bool has_value() const {
    return res.index() == 0;
  }

  bool has_error() const {
    return res.index() == 1;
  }

  V &value_unchecked() {
    return std::get<0>(res);
  }

  Err &error_unchecked() {
    return std::get<1>(res);
  }

  V &value() {
    if (res.index() != 0) {
      throw std::runtime_error{"Taking value of a result containing an error"};
    }
    return std::get<0>(res);
  }

  Err &error() {
    if (res.index() != 1) {
      throw std::runtime_error{"Taking error of a result containing a value"};
    }
    return std::get<1>(res);
  }

 private:
  std::variant<V, Err> res;
};
