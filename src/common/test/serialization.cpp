#include <common/messages.hpp>

template<typename T1, typename T2> 
void assert_eq(const T1& t1, const T2& t2) {
    if (t1 != t2) {
        std::cerr << std::format("Assertion failed: {} != {}\n", t1, t2);
    }
}

void test_int_serialization() {
    auto r1 = serialize(5ul);
    assert_eq(5ul, deserialize<size_t>(r1));
}

void test_float_serialization() {
    auto r1 = serialize(1.23f);
    assert_eq(1.23f, deserialize<float>(r1));
}

int main() {
    test_int_serialization();
    test_float_serialization();
}