#pragma once

#include <string_view>
#include <stdexcept>
#include <format>

#include <cstring>

int posix_unwrap(int result, std::string_view note = "") {
  if (result == -1) {
    if (note.empty()) {
      throw std::runtime_error{
          std::format("POSIX call failed: {}", strerror(errno))};
    } else {
      throw std::runtime_error{std::format("{}: {}", note, strerror(errno))};
    }
  }
  return result;
}
