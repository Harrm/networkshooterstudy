#pragma once

#include <sys/epoll.h>

#include <common/log.hpp>

#include "generator.hpp"
#include "unwrap.hpp"

enum class Event : uint32_t {
  READ = EPOLLIN,
  WRITE = EPOLLOUT,
};

struct SocketEvent {
  int socket;
  Event event;

  bool operator==(const SocketEvent &) const = default;
};

template <>
struct std::hash<SocketEvent> {
  std::size_t operator()(const SocketEvent &t) const noexcept {
    static_assert(sizeof(t.socket) == 4);
    return (static_cast<uint64_t>(t.socket) << 32)
         | static_cast<uint64_t>(t.event);
  }
};

class EpollService {
 public:
  EpollService() {
    epoll_fd = posix_unwrap(epoll_create1(0), "epoll_create");
  }

  void add_socket(int socket) {
    epoll_event event{
        .events = EPOLLIN | EPOLLOUT,
        .data = {
            .fd = socket,
        },
    };
    posix_unwrap(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket, &event),
                 "epoll_ctl");
  }

  Generator<SocketEvent> get_ready_sockets() {
    epoll_event events[32];
    int events_num =
        posix_unwrap(epoll_wait(epoll_fd, events, 32, 0), "epoll_wait");
    for (int event_id = 0; event_id < events_num; event_id++) {
      auto events_type = events[event_id].events;
      auto socket = events[event_id].data.fd;
      if (events_type & EPOLLIN) {
        co_yield SocketEvent{socket, Event::READ};
      }
      if (events_type & EPOLLOUT) {
        co_yield SocketEvent{socket, Event::WRITE};
      }
    }
  }

 private:
  int epoll_fd;
};