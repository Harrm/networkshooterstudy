#pragma once

#include <format>
#include <chrono>
#include <iostream>

enum class LogLevel {
    ERR, WARN, INFO, DEBUG, TRACE
};

constexpr LogLevel GlobalLogLevel = LogLevel::TRACE; 

inline std::string_view to_string(LogLevel level) {
    switch(level) {
        case LogLevel::ERR: return "Error";
        case LogLevel::WARN: return "Warning";
        case LogLevel::INFO: return "Info";
        case LogLevel::DEBUG: return "Debug";
        case LogLevel::TRACE: return "Trace";
    }
    return "Unknown";
}

constexpr inline auto to_underlying(LogLevel l) {
    return static_cast<std::underlying_type_t<LogLevel>>(l);
}

template<LogLevel level>
concept EnabledLogLevel = to_underlying(level) <= to_underlying(GlobalLogLevel);

template <LogLevel level, typename... Args> requires EnabledLogLevel<level> 
void log(std::string_view category, std::format_string<const Args&...> msg, const Args&... args) {
    auto line = std::format("\t{} - [{}] -\t{}\n", category, to_string(level), std::format(msg, args...));
    std::cout << line;
}

template <LogLevel level, typename... Args> requires (!EnabledLogLevel<level>) 
void log(std::string_view, std::format_string<const Args&...>, const Args&...) {
}

#define LOG(category, level, msg, ...) log<LogLevel::level>(#category, msg __VA_OPT__(,) __VA_ARGS__)
