#pragma once

#include <stdexcept>
#include <string_view>
#include <format>

#include <SDL2/SDL_error.h>
#include <SDL2/SDL_ttf.h>

void sdl_unwrap(int result, std::string_view note = "") {
  if (result != 0) {
    if (note.empty()) {
      throw std::runtime_error{
          std::format("SDL call failed: {}", SDL_GetError())};
    } else {
      throw std::runtime_error{std::format("{}: {}", note, SDL_GetError())};
    }
  }
}

template <typename T>
T *sdl_unwrap(T *result, std::string_view note = "") {
  if (result == nullptr) {
    if (note.empty()) {
      throw std::runtime_error{
          std::format("SDL call failed: {}", SDL_GetError())};
    } else {
      throw std::runtime_error{std::format("{}: {}", note, SDL_GetError())};
    }
  }
  return result;
}

void ttf_unwrap(int result, std::string_view note = "") {
  if (result != 0) {
    if (note.empty()) {
      throw std::runtime_error{
          std::format("TTF call failed: {}", TTF_GetError())};
    } else {
      throw std::runtime_error{std::format("{}: {}", note, TTF_GetError())};
    }
  }
}

template <typename T>
T *ttf_unwrap(T *result, std::string_view note = "") {
  if (result == nullptr) {
    if (note.empty()) {
      throw std::runtime_error{
          std::format("TTF call failed: {}", TTF_GetError())};
    } else {
      throw std::runtime_error{std::format("{}: {}", note, TTF_GetError())};
    }
  }
  return result;
}
