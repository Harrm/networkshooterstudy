#pragma once

#include <string_view>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <common/unwrap.hpp>
#include "sdl_unwrap.hpp"

void render_text(SDL_Renderer *renderer,
                 TTF_Font *font,
                 std::string_view text,
                 int x,
                 int y) {
  SDL_Surface *text_surface = ttf_unwrap(
      TTF_RenderText_Solid(font, text.data(), SDL_Color{255, 255, 0, 255}));

  SDL_Texture *text_texture =
      sdl_unwrap(SDL_CreateTextureFromSurface(renderer, text_surface));

  int text_width, text_height;
  sdl_unwrap(SDL_QueryTexture(
      text_texture, nullptr, nullptr, &text_width, &text_height));

  SDL_Rect dst_rect{x, y, text_width, text_height};
  sdl_unwrap(SDL_RenderCopy(renderer, text_texture, nullptr, &dst_rect));
}


class Renderer {
 public:
  Renderer() {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    ttf_unwrap(TTF_Init());

    font = ttf_unwrap(
        TTF_OpenFont("/usr/share/fonts/TTF/Inconsolata-Black.ttf", 16));

    window = sdl_unwrap(SDL_CreateWindow("Client",
                                         SDL_WINDOWPOS_CENTERED,
                                         SDL_WINDOWPOS_CENTERED,
                                         800,
                                         600,
                                         SDL_WINDOW_OPENGL));

    renderer =
        sdl_unwrap(SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED));
  }

  void clear() {
    sdl_unwrap(SDL_SetRenderDrawColor(renderer, 40, 40, 120, 255));

    sdl_unwrap(SDL_RenderClear(renderer));
  }

  void draw_text(std::string_view text, int x, int y) {
    render_text(renderer, font, text, x, y);
  }

  void draw_filled_rect(float x, float y, float w, float h, SDL_Color color) {
    SDL_FRect rect {
      .x = x,
      .y = y,
      .w = w,
      .h = h,
    };
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRectF(renderer, &rect);
  }

void draw_outline_rect(float x, float y, float w, float h, SDL_Color color) {
    SDL_FRect rect {
      .x = x,
      .y = y,
      .w = w,
      .h = h,
    };
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawRectF(renderer, &rect);
  }
  
  void draw_line() {}

  void present() {
    SDL_RenderPresent(renderer);
  }

 private:
  SDL_Window *window;
  SDL_Renderer *renderer;
  TTF_Font *font;
};
