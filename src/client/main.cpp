#include <chrono>
#include <coroutine>
#include <functional>
#include <iostream>
#include <list>
#include <thread>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/socket.h>

#include <assert.h>
#include <errno.h>
#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <common/async_timer.hpp>
#include <common/executor.hpp>
#include <common/game.hpp>
#include <common/log.hpp>
#include <common/messages.hpp>
#include <common/net_client.hpp>
#include <common/network_executor.hpp>
#include <common/task.hpp>
#include <common/unwrap.hpp>

#include "renderer.hpp"
#include "sdl_unwrap.hpp"

class ClientApp {
 public:
  ClientApp() : client{executor.network()} {}

  void run() {
    bool is_over = false;

    auto network_loop = [this]() -> Task<> {
      LOG(Client, INFO, "Start networking");
      auto err = co_await client.connect();
      if (err) {
        std::cerr << "Failed to connect: " << err.message() << "\n";
        std::exit(-1);
      }
      LOG(Client, INFO, "Connected successfully");

      {
        GreetingMessage msg;
        msg.player = Player{
            .id = 0,
            .name = "player",
            .color = Color{255, 0, 0},
            .pos = Vector{},
            .speed = 100.0,
        };
        auto msg_bytes = serialize(msg);
        LOG(Server, INFO, "Send greeting: {}", msg_bytes);
        LOG(Server, INFO, "Greeting: player id {}", msg.player.id);
        auto err = co_await client.send_msg(msg_bytes);
        if (err == std::errc::connection_reset
            || err == std::errc::broken_pipe) {
          throw std::runtime_error{"Connection lost"};
        }
      }

      while (true) {
        auto [err, msg_bytes] = co_await client.receive_msg();
        if (err == std::errc::connection_reset) {
          throw std::runtime_error{"Connection lost"};
        }
        auto update_msg = deserialize<GameUpdateMessage>(msg_bytes);

        for (auto &player : update_msg.players) {
          state.players[player.id] = player;
        }
      }
    };
    executor.dispatch(network_loop());

    auto render_loop = [this, &is_over]() -> Task<> {
      LOG(Client, INFO, "Start rendering");
      std::chrono::microseconds frame_time{};

      Vector player_pos{};
      float player_speed = 100.0;
      Vector player_dir{};

      while (true) {
        auto frame_start = std::chrono::steady_clock::now();
        SDL_Event event;
        float delta_time = static_cast<float>(frame_time.count()) / 1e6;

        while (SDL_PollEvent(&event)) {
          switch (event.type) {
            case SDL_QUIT:
              is_over = true;
              co_return;
            case SDL_KEYDOWN:
              switch (event.key.keysym.scancode) {
                case SDL_SCANCODE_W:
                  player_dir.y = -1.0;
                  break;
                case SDL_SCANCODE_A:
                  player_dir.x = -1.0;
                  break;
                case SDL_SCANCODE_S:
                  player_dir.y = 1.0;
                  break;
                case SDL_SCANCODE_D:
                  player_dir.x = 1.0;
                  break;
                default:
                  break;
              }
              break;
            case SDL_KEYUP:
              switch (event.key.keysym.scancode) {
                case SDL_SCANCODE_W:
                  player_dir.y = 0.0;
                  break;
                case SDL_SCANCODE_A:
                  player_dir.x = 0.0;
                  break;
                case SDL_SCANCODE_S:
                  player_dir.y = 0.0;
                  break;
                case SDL_SCANCODE_D:
                  player_dir.x = 0.0;
                  break;
                default:
                  break;
              }
              break;
            default:
              break;
          }
        }
        player_pos.x += player_dir.x * delta_time * player_speed;
        player_pos.y += player_dir.y * delta_time * player_speed;

        renderer.clear();

        std::string frame_time_str = std::format("{} us", frame_time.count());
        renderer.draw_text(frame_time_str, 15, 15);

        std::string fps_str = std::format(
            "{} FPS", frame_time.count() ? 1'000'000 / frame_time.count() : 0);
        renderer.draw_text(fps_str, 15, 50);

        renderer.draw_filled_rect(player_pos.x,
                                  player_pos.y,
                                  100.0,
                                  100.0,
                                  SDL_Color{255, 0, 0, 255});

        renderer.present();

        auto now = std::chrono::steady_clock::now();
        frame_time = std::chrono::duration_cast<std::chrono::microseconds>(
            now - frame_start);
        constexpr auto FIXED_FRAME_TIME =
            std::chrono::microseconds(static_cast<unsigned long>(1e6) / 60);
        if (frame_time < FIXED_FRAME_TIME) {
          auto sleep_time = FIXED_FRAME_TIME - frame_time;
          co_await sleep(executor.timer(), sleep_time);
          auto now = std::chrono::steady_clock::now();
          frame_time = std::chrono::duration_cast<std::chrono::microseconds>(
              now - frame_start);

        } else {
          co_await surrender(executor);
        }
      }
    };
    executor.dispatch(render_loop());

    while (!is_over) {
      executor.update();
      std::this_thread::yield();
    }
  }

 private:
  Renderer renderer;
  Executor executor;
  NetClient client;
  GameState state;
};

int main() {
  ClientApp app;
  app.run();
}
